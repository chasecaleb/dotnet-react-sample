# Sample: C#/.NET and Typescript/React
> Goal: Cross-platform stack emphasizing simplicity balanced with best-practice industry standards.

## Tech Stack
* Server: C# 7.1 with .NET Core 2.0
* Client: Typescript 2.6 with React 16.
    * Note: Redux is not used. The complexity is overkill for this project.

## Developer Setup
### Environment:
* OS: This stack is cross-platform and can be built and run on Windows/Linux/Mac/etc.
* IDE: If you don't have one in mind, [VS Code](https://code.visualstudio.com/) is cross-platform, free, easy to get started with, and works for both server and client. Visual Studio, WebStorm, Atom, etc. should all work as well.
* Backend dependency: .NET Core 2.0 SDK. See [Microsoft's Getting Started Guide](https://www.microsoft.com/net/learn/get-started).
* Client dependency: npm. See [npm install guide](https://nodejs.org/en/)

### Building:
1. Check out this repo
2. Install package dependencies
    * Server: `dotnet restore`
    * Client: `npm install`
3. Run tests
    * Server: **TODO**. Need to setup NUnit.
    * Client: `npm run test`
4. Start dev servers. These will automatically rebuild and reload when code changes.
    * Server: `dotnet watch run`
    * Client: `npm run start`